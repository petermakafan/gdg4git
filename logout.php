<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();


// Unset all of the session variables.
$_SESSION = array();


//Session Expires
if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time() - 43000, '/');
}
// Finally, destroy the session.
session_destroy();


header("Location: index.php");
?>

