<?php
session_start();
if (!isset($_SESSION['user_id'])) {
    header("Location: index.php");
}
$account = $_SESSION['account'];
$response = array();
if (isset($_POST['transfer'])) {
    if (!empty($_POST['accountTransfer']) && is_numeric($_POST['amount'])) {
        $url = sprintf("http://localhost/security/transfer.php?amount=%.2f&destaccount=%s");
        $response[] = file_get_contents($url);
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Welcome to A Secure Bank</h1>
        <h2>Transfer</h2>
        <?php
        if (!empty($response)) {
            foreach ($response as $msg) {
                echo "<p>" . $msg . "</p>";
            }
        }
        ?>
        <form action="#" method="POST">
            From: <input name="accountNumber" type="text" value="<?php echo $account; ?>" disabled="disabled" /> <br />
            To: <input name="accountTransfer" type="text" /> <br />
            Amount: <input name="amount" type="text" /> <br />
            <input name="transfer" type="submit" value="Transfer" />
        </form>
    </body>
</html>

