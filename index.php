<?php
include 'connect.php';
$errors = array();
$results = array();
if (isset($_POST['login'])) {
    if (!empty($_POST['accountNumber']) && !empty($_POST['password'])) {
        $stmt = $dbh->prepare('Select id,password from accounts where account_number = (:account)');
        $stmt->bindValue(':account', $_POST['accountNumber']);
        $stmt->execute();
        $results = $stmt->fetch();
        if ($results) {
            if ($results['password'] == $_POST['password']) {
                session_start();
                $_SESSION['user_id'] = $results['id'];
                $_SESSION['account'] = $_POST['accountNumber'];
                header("Location: main.php");
            } else {
                $errors[] = "password mismatch";
            }
        } else {
            $errors[] = "Invalid Account details";
        }
    } else {
        $errors[] = "Invalid parameters";
    }
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Welcome to all is Secure Bank</h1>
        <h2>Login </h2>
        <?php
        if (!empty($errors)) {
            foreach ($errors as $error) {
                echo "<p>" . $error . "</p>";
            }
        }
        ?>
        <form action="#" method="POST">
            Account number: <input name="accountNumber" type="text" /> <br />
            Password: <input name="password" type="text" /> <br />
            <input name="login" type="submit" value="Login" />
        </form>
    </body>
</html>
